### 코인 빨래방 기계 관리 API
***
(개인프로젝트) 코인 빨래방의 기계를 관리하는 API
***

### Language
```
JAVA 16
SpringBoot 2.7.3
```

### 기능
```
* 회원 관리
 - 회원 정보 등록하기
 - 회원 정보 리스트 가져오기
 - 회원 정보 삭제하기

* 기계 관리
 - 기계 정보 등록하기
 - 기계 정보 하나 가져오기
 - 기계 정보 리스트 가져오기
 - 기계 정보 이름 수정하기
 - 기계 정보 삭제하기
 
* 이용내역 관리
 - 이용내역 정보 등록하기
 - 기간에 해당되는 이용내역 리스트 가져오기 
```

### 예시
>* 스웨거 전체 화면
>
>![swagger-all](./images/swagger-all.png)

>* 스웨거 회원 관리 화면
>
>![swagger-member](./images/swagger-member.png)

>* 스웨거 기계 관리 화면
>
>![swagger-machine](./images/swagger-machine.png)

>* 스웨거 사용내역 관리 화면
>
>![swagger-usageDetail](./images/swagger-usagedetail.png)