package com.jth.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class UsageDetailRequest {
    @ApiModelProperty(notes = "기계 시퀀스", required = true)
    @NotNull
    private Long machineId;

    @ApiModelProperty(notes = "회원 시퀀스", required = true)
    @NotNull
    private Long memberId;

    @ApiModelProperty(notes = "이용내역 날짜", required = true)
    @NotNull
    private LocalDateTime dateUsage;
}
