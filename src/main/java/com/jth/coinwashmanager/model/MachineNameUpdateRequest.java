package com.jth.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MachineNameUpdateRequest {
    @ApiModelProperty(notes = "기계 이름", required = true)
    @NotNull
    @Length(min = 2, max = 15)
    private String machineName;
}
