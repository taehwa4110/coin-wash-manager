package com.jth.coinwashmanager.model;

import com.jth.coinwashmanager.enums.MachineType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class  MachineRequest {
    @ApiModelProperty(notes = "기계 타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MachineType machineType;

    @ApiModelProperty(notes = "기계 이름 (2~15자)", required = true)
    @NotNull
    @Length(min = 2, max = 15)
    private String machineName;

    @ApiModelProperty(notes = "기계 구매날짜", required = true)
    @NotNull
    private LocalDate datePurchase;

    @ApiModelProperty(notes = "기계 가격", required = true)
    @NotNull
    private Double machinePrice;
}
