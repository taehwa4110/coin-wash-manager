package com.jth.coinwashmanager.model;

import com.jth.coinwashmanager.entity.UsageDetails;
import com.jth.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class UsageDetailItem {
    @ApiModelProperty(notes = "이용내역 시퀀스")
    private Long usageDetailId;
    @ApiModelProperty(notes = "이용내역 날짜")
    private LocalDateTime dateUsage;
    @ApiModelProperty(notes = "기계 시퀀스")
    private Long machineId;
    @ApiModelProperty(notes = "기계 풀네임")
    private String machineFullName;
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;
    @ApiModelProperty(notes = "회원 이름")
    private String memberName;
    @ApiModelProperty(notes = "회원 전화번호")
    private String memberPhone;
    @ApiModelProperty(notes = "회원 생년월일")
    private LocalDate memberBirthday;
    @ApiModelProperty(notes = "회원 활성화 여부")
    private Boolean memberIsEnable;
    @ApiModelProperty(notes = "회원 가입일")
    private LocalDateTime memberDateJoin;
    @ApiModelProperty(notes = "회원 탈퇴일")
    private LocalDateTime memberDateWithdrawal;

    private UsageDetailItem(UsageDetailsItemBuilder builder) {
        this.usageDetailId = builder.usageDetailId;
        this.dateUsage = builder.dateUsage;
        this.machineId = builder.machineId;
        this.machineFullName = builder.machineFullName;
        this.memberId = builder.memberId;
        this.memberName  = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.memberBirthday = builder.memberBirthday;
        this.memberIsEnable = builder.memberIsEnable;
        this.memberDateJoin = builder.memberDateJoin;
        this.memberDateWithdrawal = builder.memberDateWithdrawal;
    }

    public static class UsageDetailsItemBuilder implements CommonModelBuilder<UsageDetailItem> {
        private final Long usageDetailId;
        private final LocalDateTime dateUsage;
        private final Long machineId;
        private final String machineFullName;
        private final Long memberId;
        private final String memberName;
        private final String memberPhone;
        private final LocalDate memberBirthday;
        private final Boolean memberIsEnable;
        private final LocalDateTime memberDateJoin;
        private final LocalDateTime memberDateWithdrawal;

        public UsageDetailsItemBuilder(UsageDetails usageDetails) {
            this.usageDetailId = usageDetails.getId();
            this.dateUsage = usageDetails.getDateUsage();
            this.machineId = usageDetails.getMachine().getId();
            this.machineFullName = "[" + usageDetails.getMachine().getMachineType().getName() + "]" + usageDetails.getMachine().getMachineName();
            this.memberId = usageDetails.getMember().getId();
            this.memberName = usageDetails.getMember().getMemberName();
            this.memberPhone = usageDetails.getMember().getMemberPhone();
            this.memberBirthday = usageDetails.getMember().getBirthday();
            this.memberIsEnable = usageDetails.getMember().getIsEnable();
            this.memberDateJoin = usageDetails.getMember().getDateJoin();
            this.memberDateWithdrawal = usageDetails.getMember().getDateWithdrawal();
        }

        @Override
        public UsageDetailItem build() {
            return new UsageDetailItem(this);
        }
    }

}
