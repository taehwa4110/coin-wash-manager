package com.jth.coinwashmanager.model;

import com.jth.coinwashmanager.entity.Machine;
import com.jth.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineItem {
    @ApiModelProperty(notes = "기계 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "기계 풀네임")
    private String machineFullName;
    @ApiModelProperty(notes = "기계 구매날짜")
    private LocalDate datePurchase;
    @ApiModelProperty(notes = "기계 사용 여부")
    private String isUsed;

    private MachineItem(MachineItemBuilder builder) {
        this.id = builder.id;
        this.machineFullName = builder.machineFullName;
        this.datePurchase = builder.datePurchase;
        this.isUsed = builder.isUsed;
    }

    public static class MachineItemBuilder implements CommonModelBuilder<MachineItem> {
        private final Long id;
        private final String machineFullName;
        private final LocalDate datePurchase;
        private final String isUsed;

        public MachineItemBuilder(Machine machine) {
            this.id = machine.getId();
            this.machineFullName = "[" + machine.getMachineType().getName() + "]" + machine.getMachineName();
            this.datePurchase = machine.getDatePurchase();
            this.isUsed = machine.getIsUsed() ? "사용" : "고장";
        }

        @Override
        public MachineItem build() {
            return new MachineItem(this);
        }
    }
}
