package com.jth.coinwashmanager.service;

import com.jth.coinwashmanager.entity.Machine;
import com.jth.coinwashmanager.entity.Member;
import com.jth.coinwashmanager.entity.UsageDetails;
import com.jth.coinwashmanager.model.ListResult;
import com.jth.coinwashmanager.model.UsageDetailItem;
import com.jth.coinwashmanager.repository.UsageDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailsService {
    private final UsageDetailsRepository usageDetailsRepository;

    /**
     * 이용내역 등록하기
     * @param member 고객 정보
     * @param machine 기계 정보
     * @param dateUsage 사용 날짜
     */
    public void setUsageDetail(Member member,Machine machine, LocalDateTime dateUsage) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(machine, member,  dateUsage).build();
        usageDetailsRepository.save(usageDetails);
    }

    /**
     * 기간에 해당되는 이용내역 리스트 가져오기
     * @param dateStart 시작날
     * @param dateEnd 종료날
     * @return 기간에 해당되는 이용내역 리스트
     */
    public ListResult<UsageDetailItem> getUsageDetails(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );

        LocalDateTime dateEndTime = LocalDateTime.of(dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );

        List<UsageDetails> usageDetails = usageDetailsRepository.findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);
        List<UsageDetailItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetails1 -> {
            UsageDetailItem addItem = new UsageDetailItem.UsageDetailsItemBuilder(usageDetails1).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }
}
