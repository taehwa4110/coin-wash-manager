package com.jth.coinwashmanager.service;

import com.jth.coinwashmanager.entity.Machine;
import com.jth.coinwashmanager.enums.MachineType;
import com.jth.coinwashmanager.exception.CMissingDataException;
import com.jth.coinwashmanager.model.*;
import com.jth.coinwashmanager.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    /**
     * 기계 정보 주기
     * @param id 기계 시퀀스
     * @return 기계 정보
     */
    public Machine getMachineData(long id) {
        return machineRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * 기계 등록하기
     * @param request 기계를 등록하기 위해 필요로 하는 값
     */
    public void setMachine(MachineRequest request) {
        Machine machine = new Machine.MachineBuilder(request).build();
        machineRepository.save(machine);
    }

    /**
     * 기계 상세정보 가져오기
     * @param id 기계 시퀀스
     * @return 기계 상세정보
     */
    public MachineDetail getMachine(long id) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        return new MachineDetail.MachineDetailBuilder(machine).build();
    }

    /**
     * 기계 리스트 가져오기
     * @return 기계 리스트
     */
    public ListResult<MachineItem> getMachines() {
        List<MachineItem> result = new LinkedList<>();

        List<Machine> machines = machineRepository.findAll();

//        for (Machine machine : machines) {
//            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
//            result.add(machineItem);
//        }

        machines.forEach(machine -> {
            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(machineItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 기계 종류에 따른 기계 리스트 가져오기
     * @param machineType 기계 종류
     * @return 기계 종류에 따른 기계 리스트
     */
    public ListResult<MachineItem> getMachines(MachineType machineType) {
        List<MachineItem> result = new LinkedList<>();

        List<Machine> machines = machineRepository.findAllByMachineTypeOrderByIdDesc(machineType);

        machines.forEach(machine -> {
            MachineItem machineItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(machineItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 기계 이름 정보 수정하기
     * @param id 기계 시퀀스
     * @param request 기계 이름을 수정하기 위해 필요로 하는 값
     */
    public void putMachineName(long id, MachineNameUpdateRequest request) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putDataName(request);

        machineRepository.save(machine);
    }

    /**
     * 기계 정보 삭제하기
     * @param id 기계 시퀀스
     */
    public void putMachineIsUsed(long id) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putIsUsed();

        machineRepository.save(machine);
    }


}
