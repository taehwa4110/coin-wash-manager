package com.jth.coinwashmanager.service;

import com.jth.coinwashmanager.entity.Member;
import com.jth.coinwashmanager.exception.CMissingDataException;
import com.jth.coinwashmanager.exception.CNoMemberDataException;
import com.jth.coinwashmanager.model.ListResult;
import com.jth.coinwashmanager.model.MemberItem;
import com.jth.coinwashmanager.model.MemberJoinRequest;
import com.jth.coinwashmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    /**
     * 회원 정보 주기
     * @param id 회원 시퀀스
     * @return 회원 정보
     */
    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    /**
     * 회원 등록하기
     * @param joinRequest 회원을 등록하기 위해 필요로 하는 값
     */
    public void setMember(MemberJoinRequest joinRequest) {
        Member member = new Member.MemberBuilder(joinRequest).build();
        memberRepository.save(member);
    }

    /**
     * 회원 리스트 가져오기
     * @return 회원 리스트
     */
    public ListResult<MemberItem> getMembers() {
        List<MemberItem> result = new LinkedList<>();

        List<Member> members = memberRepository.findAll();

        members.forEach(member -> {
            MemberItem memberItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(memberItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 활성화 여부에 따라 회원 리스트 가져오기
     * @param isEnable 활성화 여부
     * @return 활성화 여부에 따른 회원 리스트
     */
    public ListResult<MemberItem> getMembers(boolean isEnable) {
        List<Member> members = memberRepository.findAllByIsEnableOrderByIdDesc(isEnable);

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem memberItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(memberItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 회원 정보 삭제하기
     * @param id 회원 시퀀스
     */
    public void putMemberWithdrawal(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw new CNoMemberDataException();

        member.putWithdrawal();
        memberRepository.save(member);
    }
}
