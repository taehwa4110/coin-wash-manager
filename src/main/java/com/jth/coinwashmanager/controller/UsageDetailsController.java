package com.jth.coinwashmanager.controller;

import com.jth.coinwashmanager.entity.Machine;
import com.jth.coinwashmanager.entity.Member;
import com.jth.coinwashmanager.model.CommonResult;
import com.jth.coinwashmanager.model.ListResult;
import com.jth.coinwashmanager.model.UsageDetailItem;
import com.jth.coinwashmanager.model.UsageDetailRequest;
import com.jth.coinwashmanager.service.MachineService;
import com.jth.coinwashmanager.service.MemberService;
import com.jth.coinwashmanager.service.ResponseService;
import com.jth.coinwashmanager.service.UsageDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "이용내역 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-details")
public class UsageDetailsController {
    private final UsageDetailsService usageDetailsService;
    private final MachineService machineService;
    private final MemberService memberService;

    @ApiOperation(value = "이용내역 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setUsageDetail(@RequestBody @Valid UsageDetailRequest request) {
        Machine machine = machineService.getMachineData(request.getMachineId());
        Member member = memberService.getMemberData(request.getMemberId());
        usageDetailsService.setUsageDetail(member, machine, request.getDateUsage());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기간에 해당되는 이용내역 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<UsageDetailItem> getUsageDetails(
            @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateStart,
            @RequestParam(value = "dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate dateEnd
            ) {
        return ResponseService.getListResult(usageDetailsService.getUsageDetails(dateStart, dateEnd), true);
    }
}
