package com.jth.coinwashmanager.controller;

import com.jth.coinwashmanager.model.CommonResult;
import com.jth.coinwashmanager.model.ListResult;
import com.jth.coinwashmanager.model.MemberItem;
import com.jth.coinwashmanager.model.MemberJoinRequest;
import com.jth.coinwashmanager.service.MemberService;
import com.jth.coinwashmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 관리 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "회원 정보 등록하기")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest joinRequest) {
        memberService.setMember(joinRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 정보 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<MemberItem> getMembers(@RequestParam(value = "isEnable", required = false) Boolean isEnable) {
        if (isEnable == null) return ResponseService.getListResult(memberService.getMembers(), true);
        else return ResponseService.getListResult(memberService.getMembers(isEnable), true);
    }

    @ApiOperation(value = "회원 정보 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "회원 시퀀스", required = true)
    })
    @DeleteMapping("/{id}")
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberWithdrawal(id);

        return ResponseService.getSuccessResult();
    }
}
