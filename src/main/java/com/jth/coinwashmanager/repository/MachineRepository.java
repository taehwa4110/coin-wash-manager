package com.jth.coinwashmanager.repository;

import com.jth.coinwashmanager.entity.Machine;
import com.jth.coinwashmanager.enums.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findAllByMachineTypeOrderByIdDesc(MachineType machineType);
}
