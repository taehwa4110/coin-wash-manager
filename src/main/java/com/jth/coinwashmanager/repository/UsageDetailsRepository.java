package com.jth.coinwashmanager.repository;

import com.jth.coinwashmanager.entity.UsageDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface UsageDetailsRepository extends JpaRepository<UsageDetails, Long> {
    List<UsageDetails> findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
}
