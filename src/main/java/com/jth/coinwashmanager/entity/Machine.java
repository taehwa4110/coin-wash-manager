package com.jth.coinwashmanager.entity;

import com.jth.coinwashmanager.enums.MachineType;
import com.jth.coinwashmanager.interfaces.CommonModelBuilder;
import com.jth.coinwashmanager.model.MachineNameUpdateRequest;
import com.jth.coinwashmanager.model.MachineRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Machine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private MachineType machineType;

    @Column(nullable = false, length = 15)
    private String machineName;

    @Column(nullable = false)
    private LocalDate datePurchase;

    @Column(nullable = false)
    private Double machinePrice;

    @Column(nullable = false)
    private Boolean isUsed;

    @Column(nullable = false)
    private LocalDateTime dateUse;

    private LocalDateTime dateDisposal;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putIsUsed() {
        this.isUsed = false;
        this.dateDisposal = LocalDateTime.now();
    }

    public void putDataName(MachineNameUpdateRequest request) {
        this.machineName = request.getMachineName();
    }

    private Machine(MachineBuilder machineBuilder) {
        this.machineType = machineBuilder.machineType;
        this.machineName = machineBuilder.machineName;
        this.datePurchase = machineBuilder.datePurchase;
        this.machinePrice = machineBuilder.machinePrice;
        this.isUsed = machineBuilder.isUsed;
        this.dateUse = machineBuilder.dateUse;
        this.dateCreate = machineBuilder.dateCreate;
        this.dateUpdate = machineBuilder.dateUpdate;
    }

    public static class MachineBuilder implements CommonModelBuilder<Machine> {
        private final MachineType machineType;
        private final String machineName;
        private final LocalDate datePurchase;
        private final Double machinePrice;
        private final Boolean isUsed;
        private final LocalDateTime dateUse;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MachineBuilder(MachineRequest request) {
            this.machineType = request.getMachineType();
            this.machineName = request.getMachineName();
            this.datePurchase = request.getDatePurchase();
            this.machinePrice = request.getMachinePrice();
            this.isUsed = true;
            this.dateUse = LocalDateTime.now();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();

        }

        @Override
        public Machine build() {
            return new Machine(this);
        }
    }

}
