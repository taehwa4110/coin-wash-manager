package com.jth.coinwashmanager.entity;

import com.jth.coinwashmanager.interfaces.CommonModelBuilder;
import com.jth.coinwashmanager.model.MemberJoinRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false, length = 20)
    private String memberPhone;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false)
    private Boolean isEnable;

    @Column(nullable = false)
    private LocalDateTime dateJoin;

    private LocalDateTime dateWithdrawal;

    public void putWithdrawal() {
        this.isEnable = false;
        this.dateWithdrawal = LocalDateTime.now();
    }

    private Member(MemberBuilder memberBuilder) {
        this.memberName = memberBuilder.memberName;
        this.memberPhone = memberBuilder.memberPhone;
        this.birthday = memberBuilder.birthday;
        this.isEnable = memberBuilder.isEnable;
        this.dateJoin = memberBuilder.dateJoin;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String memberName;
        private final String memberPhone;
        private final LocalDate birthday;
        private final Boolean isEnable;
        private final LocalDateTime dateJoin;


        public MemberBuilder(MemberJoinRequest joinRequest) {
            this.memberName = joinRequest.getMemberName();
            this.memberPhone = joinRequest.getMemberPhone();
            this.birthday = joinRequest.getBirthday();
            this.isEnable = true;
            this.dateJoin = LocalDateTime.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
