package com.jth.coinwashmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
